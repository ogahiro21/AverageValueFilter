#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "def.h"
#include "var.h"
#include "bmpfile.h"

#define NUM 5
#define SIZE 256

// オペレータに値を入れる関数
void set_ope(double ope[][3]);
// 注目画素の更新値を求める関数
int  calc_around_pixel(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                                double ope[][3], int x, int y, int color);

int main(int argc, char *argv[])
{
  imgdata idata;
  double ope[3][3];         // オペレーター
  int x, y;
  int around_pixel = 8;

  if (argc < 3) printf("使用法：cpbmp コピー元.bmp コピー先.bmp\n");
  else {
    if (readBMPfile(argv[1], &idata) > 0)
    printf("指定コピー元ファイル%sが見つかりません\n",argv[1]);
    else {
      set_ope(ope);
      for (y = 0; y < idata.height; y++) {
        for (x = 0; x <idata.width; x++) {
          if(x%(SIZE-1) == 0 && y%(SIZE-1) == 0) around_pixel = 3;
          else if(x%(SIZE-1) == 0 || y%(SIZE-1) == 0) around_pixel = 5;
          else around_pixel = 8;

          idata.results[RED][y][x]
          = calc_around_pixel(idata.source, ope, x, y, RED) / (NUM + around_pixel);
          idata.results[GREEN][y][x]
          = calc_around_pixel(idata.source, ope, x, y, GREEN) / (NUM + around_pixel);
          idata.results[BLUE][y][x]
          = calc_around_pixel(idata.source, ope, x, y, BLUE) / (NUM + around_pixel);
        }
      }

      if (writeBMPfile(argv[2], &idata) > 0)
      printf("コピー先ファイル%sに保存できませんでした\n",argv[2]);
    }
  }
}

void set_ope(double ope[][3]){
  int i, j;
  for(i = 0; i < 3; i++){
    for (j = 0; j < 3; j++) {
      ope[i][j] = 1;
    }
  }
  ope[1][1] = 5;
}

int calc_around_pixel(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                                double ope[][3], int x, int y, int color){
  int i, j, sum = 0;
  for (i = -1; i < 2; i++) {
    for (j = -1; j < 2; j++) {
      if (y+i < SIZE && y+i >=0 && x+j < SIZE && x+j >=0 ) {
        sum += source[color][y+i][x+j] * ope[i+1][j+1];
      }
    }
  }
  return sum;
}
