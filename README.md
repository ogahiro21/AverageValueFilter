# The average value filter

## Description
* 入力画像平均値フィルタ処理をする(n=5，Nは領域の大きさで変化)
* 処理ウィンドウサイズは3×3とする。
* 画像領域内の値だけで行う。
* 出力値は0~255となるように処理する。

## Average value filter
- 入力画像  
![in](https://gitlab.com/ogahiro21/The_average_value_filter/raw/image/image/in9.jpeg)  
- 出力画像  
![out](https://gitlab.com/ogahiro21/The_average_value_filter/raw/image/image/out9.jpeg)

## Usage
**コンパイル**
```
gcc -o cpbmp average_filter.c bmpfile.o -lm
```
**画像の出力**
```
./cpbmp in9.bmp ans9.bmp
```
